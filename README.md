 # BUILD THE IMAGE
 `docker build -t devops-front:v1 .`

 # RUN LOCALLY
 `docker run -it --rm -w /app -v $(pwd)/devops-front-app:/app -p 4200:4200 devops-front:latest ng serve --host 0.0.0.0`
 
 # RUN FROM DOKCERHUB
 `docker run -it --rm -w /app -v $(pwd)/devops-front-app:/app -p 4200:4200 crousty/devops-front:v1 ng serve --host 0.0.0.0`